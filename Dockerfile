FROM node:10.13.0-alpine

COPY . /app
WORKDIR /app

RUN apk add --no-cache --virtual .build-deps make gcc g++ python \
	&& npm i --unsafe-perm \
	&& npm run build \
	&& apk del .build-deps \
	&& rm -r node_modules

ENTRYPOINT ["npm", "start"]