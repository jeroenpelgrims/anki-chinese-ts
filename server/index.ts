import express from 'express';
import fetch from 'node-fetch';
import path from 'path';

const app = express();

app.get('/api/strokes', ((req, res) => {
	const url = `http://www.strokeorder.info/mandarin.php?q=${encodeURIComponent(req.query.character)}`;
	fetch(url)
		.then(response => response.text())
		.then(text => {
			const match = text.match(/src="(http:\/\/\w+\.strokeorder.info\/characters\/\d+\.gif)"/);
			const url = match ? match[1] : undefined;
			res.send(url);
		})
}));

const staticFolder = path.resolve(__dirname, '../build');
app.use('/:characters', express.static(staticFolder));
app.use('/', express.static(staticFolder));

const port = process.env.PORT || 3001;
app.listen(port, () => {
	console.log(`API listening on port ${port}`);
});
