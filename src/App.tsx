import React, { Component } from 'react';
import { STYLE_TONE, STYLE_TONE2 } from 'pinyin';
import InputSection from './sections/InputSection';
import PinyinSection from './sections/PinyinSection';
import SoundSection from './sections/SoundSection';
import StrokeOrderSection from './sections/StrokeOrderSection';
import { RouteComponentProps } from 'react-router-dom';
import './App.scss';

export interface RouteProps {
	characters?: string;
}

interface Props {
	characters: string[];
}

interface State {
	initialTextShown: boolean;
}

class App extends Component<Props & RouteComponentProps, State> {
	state: State = {
		initialTextShown: false
	}

	componentDidMount() {
		const { characters } = this.props;

		if (characters.length === 0 && !this.state.initialTextShown) {
			this.setState({ initialTextShown: true }, () => {
				this.props.history.push('/你好我叫若恩我不是龟');
			});
		}
	}

	render() {
		const { characters } = this.props;

		return (
			<main className="container">
				<InputSection
					characters={characters.join('')}
					onChange={characters => this.props.history.push(`/${characters}`)}
				/>
				<PinyinSection
					title="Pinyin"
					characters={characters}
					style={STYLE_TONE}
				/>
				<PinyinSection
					title="Pinyin (numbers)"
					characters={characters}
					style={STYLE_TONE2}
				/>
				<SoundSection characters={characters} />
				<StrokeOrderSection characters={characters} />
			</main>
		);
	}
}

export default App;
