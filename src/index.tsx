import React from 'react';
import ReactDom from 'react-dom';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	RouteComponentProps
} from 'react-router-dom';
import App, { RouteProps as AppRouteProps } from './App';

ReactDom.render((
	<Router>
		<Switch>
			<Route path="/:characters?" render={(props: RouteComponentProps<AppRouteProps>) => {
				const rawCharacters = props.match.params.characters || '';
				const characters = rawCharacters.split('');
				return <App characters={characters} {...props} />;
			}} />
		</Switch>
	</Router>
), document.getElementById('root'));