import React, { Component } from "react";
import './style.scss';

interface Props {
	characters: string;
	onChange: (characters: string) => void;
}

export default class InputSection extends Component<Props> {
	onChange(e: React.ChangeEvent<HTMLInputElement>) {
		const value = e.currentTarget.value || '';
		this.props.onChange(value);
	}

	render() {
		return (
			<section>
				<label>
					Input (Simplified chinese characters)
				</label>
				<input
					value={this.props.characters}
					placeholder="Write some Chinese"
					onChange={this.onChange.bind(this)}
					id="characters"
				/>
			</section>
		);
	}
}