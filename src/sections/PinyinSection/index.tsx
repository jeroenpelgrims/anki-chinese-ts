import React, { Component } from "react";
import pinyin from 'pinyin';
import './style.scss';

interface Props {
	title: string;
	characters: string[];
	style: number;
}

export default class PinyinSection extends Component<Props> {
	render() {
		const { title, characters, style } = this.props;
		const pinyinResult = pinyin(characters.join(''), { style });

		return (
			<section className="pinyin">
				<label>{title}</label>
				<div>
					{pinyinResult.join(' ')}
				</div>
			</section>
		);
	}
}