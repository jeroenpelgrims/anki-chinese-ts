import React, { Component } from "react";
import pinyin from 'pinyin';
import speakerIcon from './Speaker_Icon.svg';
import './style.scss';

function AudioIcon({ alt, url }: { alt: string; url: string }) {
	return (
		<a href={url} target="_blank" rel="noreferrer">
			<img
				src={speakerIcon}
				alt={alt}
				title={alt}
				className="audioIcon"
			/>
		</a>
	);
}

interface Props {
	characters: string[];
}

export default class SoundSection extends Component<Props> {
	get charactersString() {
		return this.props.characters.join('');
	}

	get googleSoundUrl() {
		return `https://translate.google.com/translate_tts?ie=UTF-8&tl=zh-CN&client=tw-ob&q=${this.charactersString}`;
	}

	get yellowbridgeSoundUrl() {
		const pinyinNumbers = pinyin(this.charactersString, { style: pinyin.STYLE_TONE2 })
		const concatenated = pinyinNumbers.join('_');
		return `https://www.yellowbridge.com/gensounds/py/${concatenated}.mp3`;
	}

	render() {
		return (
			<section>
				<label>Sound</label>
				{
					this.props.characters.length
						? (
							<React.Fragment>
								<AudioIcon alt="Google audio" url={this.googleSoundUrl} />
								<AudioIcon alt="Yellowbridge audio" url={this.yellowbridgeSoundUrl} />
							</React.Fragment>
						)
						: null
				}
			</section>
		);
	}
}