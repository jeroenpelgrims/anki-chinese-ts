import queryString from 'query-string';
import mem from 'mem';

interface WikipediaApiPage {
	pageid: number;
	ns: number;
	title: string;
	imagerepository: string;
	imageinfo: {
		url: string;
		descriptionurl: string;
		descriptionshorturl: string;
	}[]
}

interface WikipediaApiMissingPage {
	pageid: number;
	ns: number;
	title: string;
	missing: undefined;
}

interface WikipediaApiResponse {
	batchcomplete: undefined;
	query: {
		pages: {
			[id: string]: (WikipediaApiPage | WikipediaApiMissingPage)
		}
	}
}

async function strokeOrderWikipedia(character: string) {
	const params = {
		action: 'query',
		format: 'json',
		origin: '*',
		prop: 'imageinfo',
		iwurl: 1,
		titles: `File:${character}-order.gif`,
		iiprop: 'url'
	};
	const url = `https://commons.wikimedia.org/w/api.php?${queryString.stringify(params)}`;
	const response: WikipediaApiResponse = await fetch(url).then(r => r.json());
	const pages = response.query.pages;
	const key = Object.keys(pages)[0];

	if (key === '-1') {
		throw response;
	} else {
		const page = pages[key] as WikipediaApiPage;
		return page.imageinfo[0].url;
	}
}

function strokeOrderBiShun(character: string) {
	const url = `/api/strokes?character=${character}`;
	return fetch(url).then(response => response.text());
}

function _strokeOrderImageUrl(character: string) {
	return strokeOrderWikipedia(character).catch(() => strokeOrderBiShun(character));
}
export const strokeOrderImageUrl = mem(_strokeOrderImageUrl);