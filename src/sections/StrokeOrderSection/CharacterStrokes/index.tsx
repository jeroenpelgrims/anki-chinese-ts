import React from 'react';
import { strokeOrderImageUrl } from './api';
import './style.scss';

interface Props {
	character: string;
}

interface State {
	url: string | undefined;
	loadingUrl: boolean;
}

export default class CharacterStrokes extends React.Component<Props, State> {
	state: State = {
		url: undefined,
		loadingUrl: true
	}

	async componentDidMount() {
		const url = await strokeOrderImageUrl(this.props.character);
		this.setState({ url, loadingUrl: false });
	}

	renderCharacter() {
		const { character } = this.props;

		if (this.state.loadingUrl) {
			return <span className="loading">{character}</span>;
		}

		return (
			<a href={this.state.url} target="_blank">
				<img src={this.state.url} alt={character} />
			</a>
		);
	}

	render() {
		return (
			<div className="characterStrokes">
				{this.renderCharacter()}
			</div>
		);
	}
}