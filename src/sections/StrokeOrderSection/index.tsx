import React from "react";
import CharacterStrokes from './CharacterStrokes';

interface Props {
	characters: string[];
}

export default (props: Props) => {
	return (
		<section>
			<label>
				Stroke Order
			</label>
			{
				props.characters.map((character, i) =>
					<CharacterStrokes
						key={`${i}-${character}`}
						character={character}
					/>
				)
			}
		</section>
	);
}