declare module 'contains-chinese' {
	export default function containsChinese(str: string): boolean;
}